<?php
/**
 * @file
 * Contains 
 */

namespace Drupal\push_notifications_restful\Plugin\resource\push_notification\v1;
use Drupal\restful\Plugin\resource\Resource;
use Drupal\restful\Http\RequestInterface;
use Drupal\push_notifications_restful\Plugin\resource\push_notification\PushNotificationResourceValidator;
use Drupal\restful\Exception\NotFoundException;
use Drupal\restful\Exception\BadRequestException;

/**
 * Class PushNotification
 * @package Drupal\push_notifications_restful\Plugin\resource
 *
 * @Resource(
 *   name = "push-notification:1.0",
 *   resource = "push-notification",
 *   label = "Push notifications",
 *   description = "Create / delete push notifications.",
 *   authenticationTypes = {
 *     "cookie"
 *   },
 *   formatter = "json",
 *   majorVersion = 1,
 *   minorVersion = 0
 * )
 */
class PushNotification_1_0 extends Resource {

  /**
   * {@inheritdoc}
   */
  public function controllersInfo() {
    return array(
      '' => array(
        // POST.
        RequestInterface::METHOD_POST => 'createToken',
        RequestInterface::METHOD_DELETE => 'deleteToken',
        RequestInterface::METHOD_OPTIONS => 'discover',
      )
    );
  }

  /**
   * {@inheritdoc}
   */
  public function discover($path = NULL) {
    return array(
      'fields' => array(
        'token' => array(
          'info' => t('Device token'),
          'required' => TRUE,
          'methods' => array(RequestInterface::METHOD_POST, RequestInterface::METHOD_DELETE),
        ),
        'type' => array(
          'info' => t('Device type'),
          'required' => TRUE,
          'methods' => array(RequestInterface::METHOD_POST),
        ),
        'language' => array(
          'info' => t('Language'),
          'required' => FALSE,
          'methods' => array(RequestInterface::METHOD_POST),
        ),
      )
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function publicFields() {
    return array();
  }

  /**
   * Validate data sent.
   */
  public function validate() {
    $request = $this->getRequest();
    PushNotificationResourceValidator::validate($request->getParsedBody(), $request->getMethod());
  }

  /**
   * Service callback to store a device token.
   *
   * @param $data
   *   Array with the following keys:
   *   - token
   *   - type
   *
   * @return
   *   Service data
   */
  public function createToken() {
    $data = $this->getRequest()->getParsedBody();
    $this->validate();
    // Default language to English and validate language setting.
    if (isset($data['language'])) {
      // Make sure this is a valid language code.
      include_once DRUPAL_ROOT . '/includes/iso.inc';
      $languages = _locale_get_predefined_list();
      if (!array_key_exists($data['language'], $languages)) {
        throw new NotFoundException(t('This is not a valid ISO 639 language code'));
      }

      // Optionally, only allow enabled languages.
      if (variable_get('push_notifications_require_enabled_language')) {
        $available_languages = language_list();
        if (!array_key_exists($data['language'], $available_languages)) {
          throw new NotFoundException(t('This language is not enabled'));
        }
      }
      $language = $data['language'];
    }
    else {
      $default_language = language_default();
      $language = $default_language->language;
    }

    // Decode data.
    $token = $data['token'];
    $type = $data['type'];

    // Get the current user id.
    $uid = $GLOBALS['user']->uid;
    // Remove empty spaces from the token.
    $token = str_replace(' ', '', $token);
    // Convert type to integer value.
    if ($type != 'ios' && $type != 'android') {
      throw new BadRequestException(t('Type not supported.'));
    }
    else {
      $type_id = ($type == 'ios') ? PUSH_NOTIFICATIONS_TYPE_ID_IOS : PUSH_NOTIFICATIONS_TYPE_ID_ANDROID;
    }

    // Determine if this token is already registered with the current user.
    if (push_notifications_find_token($token, $uid)) {
      return array(
        'success' => 1,
        'message' => 'This token is already registered to this user.'
      );
    }

    // Store this token in the database.
    $result = push_notifications_store_token($token, $type_id, $uid, $language);

    if ($result === FALSE) {
      throw new BadRequestException(t('This token could not be stored.'));
    }
    else {
      return array(
        'success' => 1,
        'message' => 'This token was successfully stored in the database.'
      );
    }
  }

  /**
   * Deletes an already registered token.
   *
   * @param $data
   *
   * @return array|mixed
   */
  function deleteToken() {
    $data = $this->getRequest()->getParsedBody();
    $token = $data['token'];
    $this->validate();
    push_notifications_purge_token($token);
    return array(
      'success' => 1,
      'message' => 'The token was successfully removed from the database.'
    );
  }

}
