<?php
/**
 * Contains Drupal\push_notifications_restful\Plugin\resource\push_notification\PushNotificationResourceValidator.
 */

namespace Drupal\push_notifications_restful\Plugin\resource\push_notification;
use Drupal\restful\Http\RequestInterface;
use Drupal\restful\Exception\BadRequestException;

/**
 * Validate push notification resource.
 */
class PushNotificationResourceValidator {

  /**
   * Body from request.
   *
   * @var array
   */
  protected $data;

  /**
   * Data.
   *
   * @param array $data
   *   Body from request.
   */
  public function __construct($data) {
    $this->data = $data;
  }

  /**
   * Validate push notification request.
   *
   * Different validation switch method.
   * 
   * @param array $data
   *   Post request data.
   * @param string $method
   *   HTTP method.
   */
  public static function validate($data, $method) {
    $validator = new static($data);
    switch($method) {
      case RequestInterface::METHOD_POST:
        $validator->validateCreate();
        break;
      case RequestInterface::METHOD_DELETE:
        $validator->validateDelete();
        break;
    }
  }

  /**
   * Validate arguments required are present.
   *
   * @param array $args
   *   Arguments required.
   * @throws BadRequestException
   *   Exception when are missing.
   */
  public function validateRequiredArgs($args) {
    $missing_args = array();
    foreach ($args as $arg) {
      if (empty($this->data[$arg])) {
        $missing_args[] = $arg;
      }
    }

    if (!empty($missing_args)) {
      throw new BadRequestException(format_plural(
        count($missing_args),
        'Missing required argument: @arguments',
        'Missing required arguments: @arguments',
        array('@arguments' => implode(', ', $missing_args))
      ));
    }
  }

  /**
   * Validate POST request.
   */
  public function validateCreate() {
    $this->validateRequiredArgs(array('token', 'type'));
  }

  /**
   * Validate DELETE request.
   */
  public function validateDelete() {
    $this->validateRequiredArgs(array('token'));
  }
}